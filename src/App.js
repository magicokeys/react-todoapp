import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "./components/layout/header";
import Todos from "./components/Todos";
import AddTodo from "./components/AddTodo";
import About from "./components/pages/About";
import uuid from 'react-uuid';
import './App.css';

class App extends Component {

  state = {

    todos: [
      {
        id: uuid(),
        title: "Watch PinoyFreeCoder Channel.",
        completed: false
      },
      {
        id: uuid(),
        title: "Practice coding.",
        completed: false
      },
      {
        id: uuid(),
        title: "Create ReactJS application.",
        completed: false
      }
    ]


  }

  //Mark complete

  markComplete = id => {
    this.setState({
      todos: this.state.todos.map(todo => {
        if(todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    });
  };

  //delTodo
  DelTodo = id => {
    this.setState({
      todos: [...this.state.todos.filter(todo => todo.id !== id)]
    });
  };

  AddTodo = (title) => {
    const newTodo = {
      id: uuid(),
      title,
      completed: false
    };
    this.setState({ todos: [...this.state.todos, newTodo] });
  };

    render() {
        return (
          <Router>
            <div className="App">
            <Header />
            <Route 
              exact
              path="/" 
              render={(props) => (
                <React.Fragment>
                  <div className="container">
                    <AddTodo AddTodo={this.AddTodo}/>
                    <Todos 
                      todos = {this.state.todos}
                      markComplete = {this.markComplete}
                      DelTodo = {this.DelTodo}
                    />
                  </div>
                </React.Fragment>
              )}
            />
            <Route path="/about" component={About} />
          </div>
        </Router>
      )
    }

} 


export default App;
